### Технологии разработки программного обеспечения

Лабораторная работа №1: создание микросервиса на Spring Boot с базой данных

Языкова Дарья Андреевна МАС2131

Цель лабораторной работы: знакомство с проектированием многослойной архитектуры Web-API (веб-приложений, микро-сервисов).

Для начала работы

1. Склонируйте репозиторий в папку сборки (git clone адресс проекта)
1. Зайдите в склонированную директорию и выполните команду mvn package
1. Для сборки docker образа выполните команду docker build . -t <имя>:<версия>
1. Для запуска docker контейнера с мапингом портов выплнить docker run -p 8080:8080 <имя>:<версия>

Обращение к ендпоинтам:

http://localhost:8080/api/v1/products/2,

http://localhost:8080/api/v1/products/1,

http://localhost:8080/api/v1/products,

http://localhost:8080/api/v1/products/2

Ендпоинт возварщающий hostname:
http://localhost:8080/api/v1/status.
