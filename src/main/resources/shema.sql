DROP TABLE IF EXISTS GIBDD CASCADE;
DROP SEQUENCE IF EXISTS global_seq;

CREATE SEQUENCE global_seq START WITH 1;

CREATE TABLE GIBDD(
    id          INTEGER PRIMARY KEY DEFAULT  nextval('global_seq'),
    lastname        VARCHAR         NOT NULL,
    firstname       VARCHAR         NOT NULL,
    post       VARCHAR         NOT NULL,
    salary    INTEGER         NOT NULL
);